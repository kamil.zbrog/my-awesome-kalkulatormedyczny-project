package pl.kamil.zbrog.kalkulatormedyczny;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;

public class MainActivity extends AppCompatActivity {

    private TextInputEditText cisnienieSkurczoweInput, cisnienieRozkurczoweInput;
    private TextView jakieCisnienie;
    private String cisnienieS, cisnienieR;
    private double zmienna1 =0, zmienna2=0;
    private Button sprawdzB;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        cisnienieSkurczoweInput = findViewById(R.id.tex_input_1);
        cisnienieRozkurczoweInput = findViewById(R.id.tex_input_2);
        jakieCisnienie = findViewById(R.id.stan_cisnienia);
        sprawdzB = findViewById(R.id.sprawdz_button);

        sprawdzCisnienie();


    }

    private void sprawdzCisnienie()
    {

        cisnienieSkurczoweInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                cisnienieRozkurczoweInput.setEnabled(false);
                cisnienieS = cisnienieSkurczoweInput.getText().toString().trim();

                if (!(cisnienieS.isEmpty()) )
                {
                    zmienna1 = Double.valueOf(cisnienieS);

                    if( zmienna1>200)
                    {
                        Toast.makeText(MainActivity.this, "Wprowadz poprawne cisnienie", Toast.LENGTH_SHORT).show();
                        cisnienieSkurczoweInput.setText("");
                        zmienna1=0;
                        cisnienieRozkurczoweInput.setEnabled(false);
                    }
                    else
                    {
                        cisnienieRozkurczoweInput.setEnabled(true);
                    }
                    if (zmienna2>0 && zmienna1>0)
                    {

                        if ( zmienna1>110 && zmienna2>180)
                        {
                            jakieCisnienie.setText("nadcisnienie");
                        }
                        else
                        {
                            jakieCisnienie.setText("ok");
                        }


                    }

                }
                else
                {
                    zmienna1=0;


                }
            }
            @Override
            public void afterTextChanged(Editable s)
            {
            }
        });

        cisnienieRozkurczoweInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                cisnienieR = cisnienieRozkurczoweInput.getText().toString().trim();

                if (!(cisnienieR.isEmpty()) )
                {
                    zmienna2 = Double.valueOf(cisnienieR);

                    if( zmienna2>200)
                    {
                        Toast.makeText(MainActivity.this, "Wprowadz poprawne cisnienie", Toast.LENGTH_SHORT).show();
                        cisnienieRozkurczoweInput.setText("");
                        zmienna2=0;
                    }

                    if ( zmienna1>110 && zmienna2>180)
                    {
                        jakieCisnienie.setText("nadcisnienie");
                    }
                    else
                    {
                        jakieCisnienie.setText("ok");
                    }



                }
                else
                {
                    zmienna2=0;
                }
            }
            @Override
            public void afterTextChanged(Editable s)
            {
            }
        });



    }
}