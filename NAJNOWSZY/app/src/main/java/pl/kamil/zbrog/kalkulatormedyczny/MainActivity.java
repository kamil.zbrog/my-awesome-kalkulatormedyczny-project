package pl.kamil.zbrog.kalkulatormedyczny;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;

public class MainActivity extends AppCompatActivity {

    private TextInputEditText cisnienieSkurczoweInput, cisnienieRozkurczoweInput;
    private TextView jakieCisnienie;
    private String cisnienieS, cisnienieR;
    private double zmienna1 =0, zmienna2=0;
    private ImageView cisnienieBuzka;
    private double xxx;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initializeBloodPressureFields();




        sprawdzCisnienie();


    }

    private void initializeBloodPressureFields()
    {
        cisnienieSkurczoweInput = findViewById(R.id.tex_input_1);
        cisnienieRozkurczoweInput = findViewById(R.id.tex_input_2);
        jakieCisnienie = findViewById(R.id.stan_cisnienia_teks);
        cisnienieBuzka = findViewById(R.id.stan_cisnienia_image);
    }

    private void sprawdzCisnienie()
    {

        cisnienieSkurczoweInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {

            }
            @Override
            public void afterTextChanged(Editable s)
            {


                cisnienieRozkurczoweInput.setEnabled(false);
                cisnienieS = cisnienieSkurczoweInput.getText().toString().trim();

                if (!(cisnienieS.isEmpty()) )
                {
                    zmienna1 = Double.valueOf(cisnienieS);

                    if( zmienna1>200)
                    {
                        Toast.makeText(MainActivity.this, "Wprowadz poprawne cisnienie", Toast.LENGTH_SHORT).show();
                        cisnienieSkurczoweInput.setText("");
                        zmienna1=0;
                        cisnienieRozkurczoweInput.setEnabled(false);
                    }
                    else
                    {
                        cisnienieRozkurczoweInput.setEnabled(true);
                    }
                    wyswietlCisnienie(zmienna1, zmienna2);

                }
                else
                {
                    zmienna1=0;
                }
                if (cisnienieS.isEmpty())
                {
                    jakieCisnienie.setText("");
                    jakieCisnienie.setTextColor(getResources().getColor(R.color.czerwony_buzka));
                    cisnienieBuzka.setVisibility(View.INVISIBLE);
                }

            }
        });

        cisnienieRozkurczoweInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {

            }
            @Override
            public void afterTextChanged(Editable s)
            {

                cisnienieR = cisnienieRozkurczoweInput.getText().toString().trim();

                if (!(cisnienieR.isEmpty()) )
                {
                    zmienna2 = Double.valueOf(cisnienieR);

                    if( zmienna2>200)
                    {
                        Toast.makeText(MainActivity.this, "Wprowadz poprawne cisnienie", Toast.LENGTH_SHORT).show();
                        cisnienieRozkurczoweInput.setText("");
                        zmienna2=0;
                    }

                    wyswietlCisnienie(zmienna1, zmienna2);


                }
                else
                {
                    zmienna2=0;
                }
                if (cisnienieR.isEmpty())
                {
                    jakieCisnienie.setText("");
                    jakieCisnienie.setTextColor(getResources().getColor(R.color.czerwony_buzka));
                    cisnienieBuzka.setVisibility(View.INVISIBLE);
                }


            }
        });
    }

    private void wyswietlCisnienie(double a, double b)
    {

        if (a>0 && b>0)
        {


                if ( a<=90 || b<=60)
                {
                    jakieCisnienie.setText("Niskie ciśnienie krwi");
                    jakieCisnienie.setTextColor(getResources().getColor(R.color.zolty_buzka));
                    cisnienieBuzka.setImageResource(R.drawable.cisnienie_lekko_nie);
                    cisnienieBuzka.setVisibility(View.VISIBLE);
                }
                if ( a>90 && a<=120 || b>60 && b<=80)
                    {
                        jakieCisnienie.setText("Optymalne ciśnienie krwi");
                        jakieCisnienie.setTextColor(getResources().getColor(R.color.zielony_buzka));
                        cisnienieBuzka.setImageResource(R.drawable.cisnienie_dobre);
                        cisnienieBuzka.setVisibility(View.VISIBLE);
                    }

                if (a>120 && a<=140 || b>80 && b<=90)
                {
                    jakieCisnienie.setText("Podwyższone ciśnienie krwi");
                    jakieCisnienie.setTextColor(getResources().getColor(R.color.zolty_buzka));
                    cisnienieBuzka.setImageResource(R.drawable.cisnienie_lekko_nie);
                    cisnienieBuzka.setVisibility(View.VISIBLE);

                }
                if (a>140 ||  b>90 )
                {
                    jakieCisnienie.setText("Wysokie ciśnienie krwi");
                    jakieCisnienie.setTextColor(getResources().getColor(R.color.czerwony_buzka));
                    cisnienieBuzka.setImageResource(R.drawable.cisnienie_zle);
                    cisnienieBuzka.setVisibility(View.VISIBLE);
                }
        }
    }
}